#!/usr/bin/env python

import os, sys, urllib2, csv, string, time, optparse, re, socket, errno
from bioblend.galaxy import GalaxyInstance
from itertools import *
sys.path.insert( 0, os.path.dirname( __file__ ) )
#from common import get, submit, display

def create_history_name (workflow_name, sampleName, project_name):
    current_time = time.strftime("%a_%b_%d_%Y_%-I:%M:%S_%p",time.localtime(time.time()))
    history_name = "%s~%s~%s~%s" % (project_name, sampleName, workflow_name, current_time)
    return history_name

def get_src_id (mySrcName, srcItems):
    src_id = None
    for item in srcItems:
        if item['name'] == mySrcName:
            return item['id']

def get_workflow (gi, workflow_name):
    # get id for your desired workflow
    
    workflows = gi.workflows.get_workflows()

    for __workflow__ in workflows:
        if __workflow__['name'] == workflow_name:
            workflow_id = __workflow__['id']
            break

    workflow = gi.workflows.show_workflow(workflow_id)
    if not workflow:
        if return_formatted == True:
            print "Workflow %s not found, terminating." % workflow
            sys.exit(1)
    else:
        return workflow

def get_file_id (myFileName, src, src_id, gi):
    if src == 'ld':
        contents = gi.libraries.show_library(src_id, contents=True)
    else:
        contents = gi.histories.show_history(src_id, contents=True)

    for f in contents:
        if f['type'] == 'file' and myFileName in f['name']:
             return f['id']
        


def isBadLine(line):
    return line.startswith('#')

# usage: sys.argv[0] -k <api_key> -u <galaxy_url> -i <file_with_sample_info_and_parameters>
# example:  python submit_batch_workflow.py -k 32a9f8d74b8a3920ea2e89ed1ebeb27b -u http://localhost:8080 -i ~/mytest_sample_test.txt
################################################################################

#Parse Command Line
#api_url = "http://cvrg.galaxycloud.org/api/"

parser = optparse.OptionParser()
parser.add_option( '-k', '--api-key', dest='api_key', help='Mandatory. Need to get your galaxy API key by visiting the Galaxy webpage http://dev.globusgenomics.edu' )
parser.add_option( '-u', '--url', dest='url_name', help='Galaxy URL' )
parser.add_option( '-i', '--input', dest='inputBatch', help='Table containing the samples you wish to run through workflow.' )
(options, args) = parser.parse_args()

try:
    options.api_key
    options.inputBatch
    options.url_name
except NameError: 
    print 'usage: %s --api-key <api_key> --url <galaxy_url> --input <file_with_read_information>' % os.path.basename( sys.argv[0] )
    sys.exit()

batchRun = {'samples' : [] }

# Make api connection with Galaxy api server
gi = GalaxyInstance(url=options.url_name, key=options.api_key)

# Parse the input file
table_data_lines = []
table_data_flag = 0
header_flag = 0
with open(options.inputBatch, 'rU') as input_file:
    lines = (line.rstrip() for line in input_file)
    lines = (line for line in lines if line)
    for line in dropwhile(isBadLine, lines):
        if line.startswith('Workflow Name'):
            key, value = line.split("\t")
            batchRun['workflow_name'] = value
            print line
        elif line.startswith('Workflow id'):
            key, value = line.split("\t")
            batchRun['workflow_id'] = value
            print line
        elif line.startswith('Project Name'):
            key, value = line.split("\t")
            batchRun['project_name'] = value
            print line
        elif line.startswith('###TABLE DATA'):
            table_data_flag = 1
        elif table_data_flag == 1 and not line.startswith('####') and header_flag == 0:
            table_data_lines.append(line)
            print line
            header = line
            header_flag = 1
        elif table_data_flag == 1 and not line.startswith('####') and header_flag == 1:
            table_data_lines.append(line)
            print line

reader = csv.DictReader(table_data_lines, delimiter='\t')
for line in reader:
    ds_map = {}
    parameters = {}
    sampleName = None
    sub_params = {}

    for key, value in line.iteritems():
        if key.startswith('SourceType:'):
            subkeys = key.split("::")
            key_values = value.split("::")
            ds_map.update({subkeys[2]: { 'srcType': key_values[0], 'srcName': key_values[1], 'fileName': key_values[2] }})
            if ds_map[subkeys[2]]['srcType'] == 'library':
                ds_map[subkeys[2]]['srcType'] = 'ld'
            elif ds_map[subkeys[2]]['srcType'] == 'history':
                ds_map[subkeys[2]]['srcType'] = 'hda'

            # get id from library
            if ds_map[subkeys[2]]['srcType'] == "ld":
                libs = gi.libraries.get_libraries()
                src_id = get_src_id(ds_map[subkeys[2]]['srcName'], libs)     
                file_id = get_file_id (ds_map[subkeys[2]]['fileName'], ds_map[subkeys[2]]['srcType'], src_id, gi)
                print file_id
                ds_map[subkeys[2]].update({'src_id':src_id, 'file_id':file_id})                

            # get id from history
            elif ds_map[subkeys[2]]['srcType'] == "hda":
                histories = gi.histories.get_histories()
                src_id = get_src_id(ds_map[subkeys[2]]['srcName'], histories)
                print src_id
                file_id = get_file_id (ds_map[subkeys[2]]['fileName'], ds_map[subkeys[2]]['srcType'], src_id, gi)
                print file_id
                ds_map[subkeys[2]].update({'src_id':src_id, 'file_id':file_id})

        elif key.startswith('Param:'):
            param_values = key.split("::")
            key_type = param_values.pop(0)
            key_toolid = param_values.pop(0)
            key_toolName = param_values.pop(0)
            #key_type, key_toolid, key_toolName, key_toolParam = key.split("::")
            tmp_keytoolid = "step" + key_toolid

                
            if len(param_values) == 1:
                key_toolParam = param_values.pop(0)
                if tmp_keytoolid in parameters.keys():
                    parameters["step" + str(key_toolid)][key_toolName].append( {'param':key_toolParam, 'value': value} )
                else:
                    parameters["step" + str(key_toolid)] = {key_toolName : [{'param':key_toolParam, 'value': value}]} 
            elif len(param_values) == 2:
                key_toolParam = param_values.pop(0)
                key_toolSubParam = param_values.pop(0)
                if tmp_keytoolid in parameters.keys():
                    parameters["step" + str(key_toolid)][key_toolName][key_toolParam].append( {'param':key_toolSubParam, 'value': value} )
                else:
                    parameters["step" + str(key_toolid)] = {key_toolName : {key_toolParam : [{'param':key_toolSubParam, 'value': value}]}}


        elif key.startswith('SampleName'):
            sampleName = value

    # create history, move file, start workflow:

    # associate the file name with the file_id
    
    workflow = get_workflow(gi, batchRun['workflow_name'])
    #workflow = gi.workflows.show_workflow(batchRun['workflow_id'])

    print "Submitting"
    print "----------"
    print "   Workflow"
    print "      Sample: %s" % sampleName
    print "      Name: %s" % workflow['name']
    print "      ID: %s" % workflow['id']

    # create history
    history_name = create_history_name(batchRun['workflow_name'], sampleName, batchRun['project_name'])
    data = {}
    data[ 'name' ] = history_name

    print "   Inputs"

    # submit workflow with files in library into new history
    wf_data = {}
    wf_data['workflow_id'] = workflow['id']
    wf_data['history'] = history_name
    wf_data['ds_map'] = {}
    wf_data['parameters'] = parameters
    for step_id, ds_in in workflow['inputs'].iteritems():
        print ds_in
        print step_id
        ds_in['label']
        wf_data['ds_map'][step_id] = {'src':ds_map[ds_in['label']]['srcType'], 'id':ds_map[ds_in['label']]['file_id']}

        print ds_map        
        print "      Step Input ID: %s" % step_id
        print "         Input label: %s" % ds_in['label']

    print "Submitting Workflow history: %s" % history_name
    res = gi.workflows.run_workflow(wf_data['workflow_id'], wf_data['ds_map'], params=parameters, history_name=wf_data['history'], import_inputs_to_history=True)

#    try:
#	res = gi.workflows.run_workflow(wf_data['workflow_id'], wf_data['ds_map'], params=parameters, history_name=wf_data['history'], import_inputs_to_history=True)
        #if res:
            # you can loop through items in res to view the output files
            #print res
            #print "   History Name: %s" % history_name
            #print "   ID: %s" % res['history']
        #print "Submitted Workflow history: %s" %  history_name
#    except:
#        print "socket error: Disconnected prior connection, will submit next sample "

    time.sleep(5)

